# base image
FROM  node:16.9-alpine

# expose this port
EXPOSE 3000

# work-directory for the app
WORKDIR /app

# copy src files
COPY . .

# install node-modules
RUN npm install 

# issue command to run app
CMD ["npm", "start"]