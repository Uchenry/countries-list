import React, { useState } from 'react';
import { useDispatch } from 'react-redux';

import { getCountry } from '../redux/countrySlice';

const SearchForm = () => {
  const [country, setcountry] = useState('');
  const dispatch = useDispatch();

  const handleChange = ({target: { value }}) => {
    setcountry(value);
  };

  const handleSubmit = (e) => {
    e.preventDefault();
    dispatch(getCountry({name: country}));
    const form_wrapper = document.querySelector('.form_wrapper');
    const filterform = document.querySelector('.filterform');
    form_wrapper.hidden='true';
    filterform.hidden='true';
  }

  return (
    <div className = "form_wrapper">
      <form onSubmit={handleSubmit}>
        <label></label>
        <input type="text" name="country" onChange={handleChange} placeholder="search"/>
        <input type="submit"/>
      </form>
    </div>
  )
}

export default SearchForm;