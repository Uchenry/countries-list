import React, { useState } from 'react';
import { useDispatch } from 'react-redux';
import { regions } from '../regions';
import { getRegions } from '../redux/filterSlice';
import FilterStyle from './FilterFormStyle';

const FilterForm = () => {
  const [region, setRegion] = useState('');
  const dispatch = useDispatch()
  const handleChange = ({target: { value }}) => {
    setRegion(value);
  };

  const handleSubmit = (e) => {
    e.preventDefault();
    if (region === 'select a region') {
      return 
    } else {
      dispatch(getRegions(region));
    }
  }

  return (
    <FilterStyle>
      <div className="filterform">
        <form onSubmit={handleSubmit}>
          <select onChange={handleChange}>
            {regions.map(region => <option value={region} key={region}>{region}</option>)}
          </select>
          <input type="submit"/>
        </form>
      </div>
    </FilterStyle>
  )
};

export default FilterForm;