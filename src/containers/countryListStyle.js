import styled from 'styled-components'

const StyledList = styled.ul`
  display: grid;
  grid-template-columns: repeat(4, 1fr);
  grid-gap: 20px;
  padding-left: 0px;
`
export default StyledList;