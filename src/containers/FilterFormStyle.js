import styled from 'styled-components';

const FilterStyle = styled.div `
  select {
    width: 170px;
    height: 21px;
  }
  input {
    width: 70px;
  }
`;

export default FilterStyle;