import React, { useEffect } from 'react';
import { useDispatch } from 'react-redux';
import { getCountries } from '../redux/countriesSlice';
import { useSelector  } from 'react-redux';
import Country from '../components/country/country';
import StyledList from './countryListStyle';
import CountryDetail from '../components/countryDetail';

const CountryList = () => {
  let record;
  const dispatch = useDispatch()
  useEffect(() => { dispatch(getCountries())}, [dispatch]);
  const countries = useSelector((state) => state.countries);
  const country = useSelector((state) => state.country);
  console.log(country);
  const regions = useSelector((state) => state.regions);

  if(countries.status === 'success' && country.status === null && regions.status === null) {
    record = countries
    return (
      <StyledList>
        {
          record.data.map(country =>
            <Country key={country.name} 
              name={country.name}
              population={country.population}
              region={country.region}
              capital={country.capital}
            />
          )
        }
      </StyledList>
  )} 
  else if (country.status === 'success'){
    record = country.data;
    return (
      <CountryDetail 
        name={record.name}
        region={record.region}
        capital={record.capital}
      />
    )
  } 
  else {
    record = regions;
    return (
      <StyledList>
        {
          record.data.map(country =>
            <Country key={country.name}
              name={country.name}
              flag={country.flag}
              population={country.population}
              region={country.region}
              capital={country.capital}
            />
          )
        }
      </StyledList>
    )
  }
}

export default CountryList;