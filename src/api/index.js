const rootUrl = 'http://api.countrylayer.com/v2';
const getUrl = (slug) => {
  let url = rootUrl;
  url += slug ? `/${slug}`: '';
  url += `?access_key=${process.env.REACT_APP_ACCESS_KEY}`;
  return url;
}

export const getAllCountries = () => {
    return fetch(getUrl('all'))
    .then((res) => res.json()
  )
}

export const getCountry = ({name}) => {
  return fetch(getUrl(`name/${name}`))
  .then((res) => res.json())
  .then(data => (data.find(e => e.name === name)));
}

export const getRegion = (region) => {
  console.log(region)
  return fetch(getUrl(`region/${region}`))
    .then((res) => res.json())
    .then(data => (data));
}