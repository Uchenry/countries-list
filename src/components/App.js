import React from 'react';
import styled from 'styled-components';
import { Router, Route, Switch } from 'react-router-dom';
import history from '../history';
import SearchForm from '../containers/searchForm'
import CountryList from '../containers/countrylist';
import FilterForm from '../containers/filterForm';
import CountryDetail from './countryDetail';

const Container = styled.div`
  background-color: #f5f7f5;
  padding: 20px;
  font-family: Nunito Sans;
  .input-field {
    display: flex;
    justify-content: space-between;
    padding: 40px 0;
  }
`

const App = () => {
  return (
    <Container>
      <Router history={history}>
          <div className="App">
            <div className="input-field">
              <FilterForm />
              <SearchForm />
            </div>
            <Switch>
              <Route path="/" exact>
                <CountryList />
              </Route>
              <Route path='/details/:name'>
                <CountryDetail />
              </Route>
            </Switch>
          </div>
      </Router>
    </Container>
  );
}

export default App;
