import styled from 'styled-components';

const StyledDetail = styled.div`
  display: grid;
  grid-template-columns: 1fr 1fr;
  grid-template-rows: 300px;
  padding: 50px 40px 0 40px;
  .flag-wrapper {
    width: 100%;
    height: 100%;
    img {
      width: 100%;
      height: 100%;
    }
  }
  .more-info {
    .top {
      display: grid;
      grid-template-columns: 1fr 1fr;
      line-height: 1.6;
      padding-left: 40px;
      h2 {
        font-weight: 600;
        font-size: 1.6em;
        padding-top: 20px;
        height: 70px;
      }
      .right {
        padding-top: 65px;
      }
    }
    .down {
      padding: 40px 0 0 40px;
    }
  }
`;

export default StyledDetail;