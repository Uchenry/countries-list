import React from 'react';
import StyledDetail from './detailStyle';

const CountryDetail = ({ flag, nativeName, name, population, 
  region, subregion, capital, currencies, languages, topLevelDomain }) => {

  return (
    <>
      <StyledDetail>
        <div className="flag-wrapper">
          <img src={flag} alt="flag"/>
        </div>
        <div className="more-info">
          <div className="top">
            <ul className="left">
              <h2>{name}</h2>
              <li>Native Name: {nativeName}</li>
              <li>Population: {population}</li>
              <li>Region: {region}</li>
              <li>Sub Region: {subregion}</li>
              <li>Capital: {capital}</li>
            </ul>
            <ul className="right">
              <li>Top Level Domain: {topLevelDomain}</li>
              <li>Currencies: {currencies}</li>
              <li>Languages: {languages}</li>
            </ul>
          </div>
          {/* <div className="down">down</div> */}
        </div>
      </StyledDetail>
    </>
  )
};

export default CountryDetail;