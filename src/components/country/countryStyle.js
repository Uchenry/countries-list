import styled from 'styled-components'

const CountryCard = styled.div`
  width: 100%;
  height: 100%;
  box-shadow:  5px 100px 80px rgba(0, 0, 0, 0.12);
  border-radius: 5px;
  .image-wrapper {
    width: 100%;
    height: 170px;
    display: flex;
    justify-content: center;
    align-items: center;
  }
  img {
    width: 100%;
    height: 100%;
  }
  .description {
    height: 200px;
    h2 {
      margin: 0;
      padding: 20px;
      font-size: 1.4em;
      font-weight: 700;
    }
    h5 {
      margin: 0px;
      padding-left: 20px;
      line-height: 1.4;
      color: hsl(0, 0%, 52%);
      font-size: 1.0em;
    }
    span {
      font-weight: 400;
      font-weight: 600;
      color: #000;
    }
  }
`

export default CountryCard;