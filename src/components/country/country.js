import React from 'react';
import CountryCard from './countryStyle'

const Country = ({ flag, name, population, region, capital }) => {
  return (
    <>
      <CountryCard>
        <div className="country-card">
          <div className="image-wrapper">
            <img src={flag} alt="country flag" />
          </div>
          <div className="description">
            <h2>{name}</h2>
            <h5><span>Population of country: </span>{population}</h5>
            <h5><span>Region of Country: </span>{region}</h5>
            <h5><span>Capital of Country: </span>{capital}</h5>
          </div>
        </div>
    </CountryCard>
    </>
   
  )
}

export default Country;