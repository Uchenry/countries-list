import { createAsyncThunk, createSlice } from "@reduxjs/toolkit";
import { getRegion } from '../api';


export const getRegions = createAsyncThunk(
  'regions/getRegions', getRegion
);

const regionsSlice = createSlice({
  name: 'regions',
  initialState: {
    data: [],
    status: null,
  },
  extraReducers: {
    [getRegions.pending]: (state, action) => {
      state.status = 'loading'
    },
    [getRegions.fulfilled]: (state, { payload }) => {
      state.status = 'success'
      state.data = payload
    },
    [getRegions.rejected]: (state, action) => {
      state.status = 'failed'
    }
  }
});

export default regionsSlice.reducer