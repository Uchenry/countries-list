import { configureStore } from "@reduxjs/toolkit";
import countriesSlice from './countriesSlice';
import countrySlice from './countrySlice';
import filterSlice from '../redux/filterSlice';


export default configureStore({
  reducer: {
    countries: countriesSlice,
    country: countrySlice,
    regions: filterSlice
  },
})