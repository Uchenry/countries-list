import { createAsyncThunk, createSlice } from '@reduxjs/toolkit';
import { getAllCountries } from '../api';

export const getCountries = createAsyncThunk(
  'countries/getCountries', getAllCountries
)

const countriesSlice = createSlice({
  name: 'countries',
  initialState: {
    data: [],
    status: null,
  },
  extraReducers: {
    [getCountries.pending]: (state, action) => {
      state.status = 'loading'
    },
    [getCountries.fulfilled]: (state, { payload }) => {
      state.data = payload
      state.status = 'success'
    },
    [getCountries.rejected]: (state, action) => {
      state.status = 'failed'
    }
  }
});

export default countriesSlice.reducer