import { createAsyncThunk, createSlice } from "@reduxjs/toolkit";
import { getCountry as fetchCountry } from '../api'

export const getCountry = createAsyncThunk(
  'country/getCountry', fetchCountry
)

const countrySlice = createSlice({
  name: 'country',
  initialState: {
    data: [],
    status: null,
  },
  extraReducers: {
    [getCountry.pending]: (state, action) => {
      state.status = 'loading'
    },
    [getCountry.fulfilled]: (state, { payload }) => {
      state.data = payload
      state.status = 'success'
    },
    [getCountry.rejected]: (state, action) => {
      state.status = 'failed'
    }
  }
})

export default countrySlice.reducer;